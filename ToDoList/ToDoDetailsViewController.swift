import UIKit

class ToDoDetailsViewController: UIViewController {
    
    @IBOutlet weak var taskTitleLabel: UILabel!
    
    @IBOutlet weak var taskDetailsTextView: UITextView!
    
    @IBOutlet weak var taskCompletionButton: UIButton!
    
    @IBOutlet weak var taskCompletionDate: UILabel!

    var toDoIndex: Int!
    var toDoItem: ToDoItem!
    
    var delegat: ToDoListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        taskTitleLabel.text = toDoItem.name
        taskDetailsTextView.text = toDoItem.details
    
        if toDoItem.isComplete {
            disableButton()
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMM dd, yyyy hh:mm"
        let taskDate = dateFormater.string(from: toDoItem.completionDate)
        taskCompletionDate.text = taskDate
        
    }
    
    func disableButton() {
        taskCompletionButton.backgroundColor = UIColor.gray
        taskCompletionButton.isEnabled = true
    }
    
    
   
    
    @IBAction func taskDidComplete(_ sender: Any) {
        
        toDoItem.isComplete = true
        
        delegat?.update(task: toDoItem, index: toDoIndex)
        
        disableButton()
        
    }
    
}
