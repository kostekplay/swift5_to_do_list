import UIKit

protocol ToDoListDelegate {
    func update(task: ToDoItem, index: Int)
}

class ToDoListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    var toDoItems: [ToDoItem] = [ToDoItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //tableView.tableFooterView = UIView()
        title = "ToDoList"
        
        // navigartion buttons - obowiązkowy Navigation Controller
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped))
        
        let testItem1 = ToDoItem(name: "NameName1", details: "DetailDetail1", completionDate: Date())
        let testItem2 = ToDoItem(name: "NameName2", details: "DetailDetail2", completionDate: Date())
        toDoItems.append(testItem1)
        toDoItems.append(testItem2)
    }
    
    // wyłączam opcję edycji tabeli w sytuacji kiedy w czasie edycji dodam plus - czyli opuszczam vidok - nie działa
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.setEditing(false, animated: false)
    }
    
    @objc func addTapped() {
        performSegue(withIdentifier: "AddTaskSegue", sender: nil)
    }
    
    @objc func editTapped() { // zmienia rightBarButtonItem z Edit na Done
        tableView.setEditing(!tableView.isEditing, animated: true) // ważny wykrzynik
        if tableView.isEditing {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(editTapped))
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped))
        }
    }
    
    // metoda odpowiedzialna za kasowanie wiersza tabeli
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.toDoItems.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let toDoItem = toDoItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoItem", for: indexPath)
        cell.textLabel?.text = toDoItem.name
        cell.detailTextLabel?.text = toDoItem.isComplete ? "Complete" : "Incomplete"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = toDoItems[indexPath.row]
        let toDoTuple = (indexPath.row, selectedItem)
        performSegue(withIdentifier: "TaskDetailSegue", sender: toDoTuple)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TaskDetailSegue" {
         
            guard let destinationVC = segue.destination as? ToDoDetailsViewController else { return }
            guard let toDoTuple = sender as? (Int,ToDoItem) else { return }
            
            destinationVC.toDoIndex = toDoTuple.0
            destinationVC.toDoItem = toDoTuple.1
            
            destinationVC.delegat = self
        }
    }
}

extension ToDoListViewController: ToDoListDelegate {
    func update(task: ToDoItem, index: Int) {
        toDoItems[index] = task
        tableView.reloadData()
    }
}
